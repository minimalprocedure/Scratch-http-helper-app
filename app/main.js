'use strict';

/*
  Main Application
*/

const RUNMODE = {
  production: 'production',
  development: 'development'
};

const Koa = require('koa');
const App = global.App = module.exports.App = new Koa();
App.cwd = process.cwd();

/*
  Main Metadatas
*/

App.metadatas = require(`${App.cwd}/app/configs/app.conf.json`);
App.name = App.metadatas.appName;

/*
  Environment Running
*/

App.env = process.env.NODE_ENV || App.metadatas.runAsEnv || RUNMODE.development;
process.env.NODE_ENV = App.env;
App.metadatas.runAsEnv = App.env;

/*
  Default folders
*/

App.folders = {    
  routes: `${App.cwd}/app/routes`,
  modules: `${App.cwd}/app/modules`,
  app: `${App.cwd}/app`,
  public: `${App.cwd}/public`,
  scratch: `${App.cwd}/scratch`
};

/*
  Middlewares
*/

App.logger = require(`${App.folders.modules}/logger.js`);
App.helpers = require(`${App.folders.modules}/helpers.js`);
App.router = require('koa-better-router')().loadMethods();
App.bodyParser = require('koa-better-body')();
App.httpLogger = require('koa-logger');
App.serveStatic = require('koa-static');
App.R = require('ramda');
require(`${App.folders.routes}/main.js`);

App.getHttp = (options) => {
  let http = require('http');
  return new Promise((resolve, reject) => {
    let callback = (response) => {
      let str = '';
      response.on('data', chunk => str += chunk );
      response.on('end', chunk => resolve(str) );
    };
    ((options) => http.request(options, callback).end())(options);
  }); 
};

App.saveJSON = (o) => {
  var fs = require('fs');
  fs.writeFile(`${App.folders.scratch}/${App.metadatas.scratch.name}`, JSON.stringify(o), (err) => {if (err) throw err;});
};

/*
  Scratch Extension
*/

App.metadatas.scratch.extension.extensionPort = App.metadatas.server.port;
App.scratch = App.metadatas.scratch;

App.scratch.extension.blockSpecs.forEach((block) => {
  let options = App.metadatas.proxyTo;
  let [t, c, r] = block;
  let regexp = new RegExp(/%(.).(\w*)/, 'gi');
  let match;
  let params = [];
  while((match = regexp.exec(c)) !== null) params.push(`:${match[2]}`);
  let route = `/${r}${(params.length > 0) ? `/${params.join('/')}` : ""}`;
  
  App.router.addRoute("GET", route, [
    function* (next) {
      let ctx = this;
      let params = Object.keys(ctx.params).reduce((acc, k) => {return acc.concat(ctx.params[k]);}, []);      
      let json;
      switch(params[0]) {
      case "resume":
      case "pause" :                
        options.path = `/sensor/${r}${(params.length > 0) ? `/${params.join('/')}` : ""}`;
        ctx.body = yield App.getHttp(options);              
        break;
      default:
        options.path = `/sensor/${r}`;
        json = JSON.parse((yield App.getHttp(options)));
        let property = `android.sensor.${r}.${params[0]}`;
        switch (params.length) {
        case 1 :
          ctx.body = json[property].join(';');
          break;
        case 2 :
          let index = params[1] - 1;
          ctx.body = json[property][index];
          break;
        default:
          ctx.body = json[`android.sensor.${r}.raw`].join(';');
        }                
      } 
      yield next;
    }
  ]);
});

App.router.preparePollings = () => {
  let pollings = App.scratch.pollings;
  return pollings.map( polling => {
    if (polling.active) {
      return function* (next) {
        let datas = polling.datas;
        let propertyBase = `android.sensor.${polling.name}`;
        let options = App.metadatas.proxyTo;      
        let ctx = this; 
        options.path = `/sensor/${polling.name}`;
        let json = JSON.parse((yield App.getHttp(options)));
        let vars = datas.map(data => {
          let range  = [...Array(data.size).keys()];
          return (range.map( v => {           
            return `${polling.name.truncAt(5)}${data.name.capitalize().truncAt(4)}${v} ${json[`${propertyBase}.${data.name}`][v]}`;
          }).join("\n"));
        }).join("\n");
        if(ctx.body === undefined)
          ctx.body = `${vars}\n`;
        else
          ctx.body += `${vars}\n`;
        yield next;
      };
    } else {
      return function* (next) {yield next;};
    };
  });
};

App.router.addRoute("GET", "/poll", App.router.preparePollings());

// Generate reporters
Object.keys(App.scratch.pollings).forEach((poll) => {
  let polling = App.scratch.pollings[poll];
  if (polling.active) {
    polling.datas.forEach((d) => {
      let reporters = ([...Array(d.size).keys()].reduce((acc, v) => {
        let reporter = `${polling.name.truncAt(5)}${d.name.capitalize().truncAt(4)}${v}`;
        return acc.concat([["r", reporter, reporter]]);
      }, []));
      App.scratch.extension.blockSpecs = App.scratch.extension.blockSpecs.concat(reporters);
    });
    
  }
});

App.saveJSON(App.scratch.extension);

/*
  Use directives
*/
App
  .use(App.httpLogger())
  .use(App.bodyParser)
  .use(App.router.middleware())
//.use(App.router.allowedMethods({throw: true}))
  .use(App.serveStatic(App.folders.public));

/*
  Start Application
*/


App.listen(App.metadatas.server.port, () => {
  App.logger.log('Starting', App.metadatas.appName);
  App.logger.log('Environment', process.env.NODE_ENV);
});
