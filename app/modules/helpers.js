"use strict";

JSON.toSafeString = require('json-stringify-safe');

String.prototype.getRandom = function (length) {
    if(!length) length = 32;
    return require('crypto').randomBytes(length).toString('hex');
};

String.prototype.truncAt = function(size) {
    return this.substring(0,size);
};


String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Array.prototype.uniq = function() {
    if (this == null) {
        throw new TypeError('Array.prototype.uniq called on null or undefined');
    }
    return this.reduce(function(acc, curr, index, array) {
        if(acc.indexOf(curr) != -1) {       
            return acc;
        }
        else {
            return acc.concat(curr);
        }
    }, []);
};

/*
 Prepare from path like this: '/a=string/b=true/c=3/d=[1,2]/e=0/f={a:1,b:"aaa"}/g=';
 */
JSON.fromUrlPath = function(path, div) {
    let R = require('ramda');  

    function coerceValue(s) {
	let result = s;
	let v = !R.isEmpty(s) ? Number(s) : null;
	if (v < 0 || v > 0) result = v;
	else if (v == 0) result = v;
	else if (isNaN(v)){
	    if(s == 'true' || s == 'false') {
		result = Boolean(s);
	    } else {
		let isArray = R.match(/^\[(.*)\]$/, s);		
		if (!R.isEmpty(isArray)) {
		    result = R.map(function(e){
			return coerceValue(e);
		    }, R.split(',', isArray[1]));
		} else {
                    let isString = R.match(/^['|"](.*)['|"]$/, s);
                    if (!R.isEmpty(isString)) {
                        return isString[1];
                    } else {
		        let isObj = R.match(/^\{(.*)\}$/, s);                    
		        if (!R.isEmpty(isObj)) {
                            return JSON.parse(isObj[0]);
		        }
		    }
                }
	    }
	}
	return result;
    }

    let pairs = R.map( function (e) {
	let tuple = e.split('=');
        let i = tuple.length - 1;
	tuple[i] = coerceValue(tuple[i]);
	return tuple;
    }, path[0] === div ? R.tail(R.split(div, path)) : R.split(div, path));

    return R.fromPairs(pairs);
};

