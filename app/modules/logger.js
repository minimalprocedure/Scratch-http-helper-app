"use strict";

const Reset = "\x1b[0m";
const Bright = "\x1b[1m";
const Dim = "\x1b[2m";
const Underscore = "\x1b[4m";
const Blink = "\x1b[5m";
const Reverse = "\x1b[7m";
const Hidden = "\x1b[8m";

const FgBlack = "\x1b[30m";
const FgRed = "\x1b[31m";
const FgGreen = "\x1b[32m";
const FgYellow = "\x1b[33m";
const FgBlue = "\x1b[34m";
const FgMagenta = "\x1b[35m";
const FgCyan = "\x1b[36m";
const FgWhite = "\x1b[37m";

const BgBlack = "\x1b[40m";
const BgRed = "\x1b[41m";
const BgGreen = "\x1b[42m";
const BgYellow = "\x1b[43m";
const BgBlue = "\x1b[44m";
const BgMagenta = "\x1b[45m";
const BgCyan = "\x1b[46m";
const BgWhite = "\x1b[47m";


var util = require('util');
var Console = require('console').Console;
var logger = module.exports = new Console(process.stdout, process.stderr);

logger.debugLevel = 'warn';
logger.log = function() {
    let R = App.R;
    var levels = ['DEBUG', 'DEBUGDIR', 'ERROR', 'WARN', 'INFO'];
    let args = R.values(arguments);
    let level = args[0];
    args = args.slice(1);
    args = R.map(function(e){
        return !R.is(String, e) ? JSON.toSafeString(e) : e;        
    }, R.is(Object, args[0]) ? R.values(args[0]) : args);
    let msg1 = args.join(': ');    
    if (levels.indexOf(level) >= levels.indexOf(logger.debugLevel) ) {
        var output = '';
        if(level != 'DEBUGDIR') {
            if (R.is(String, msg1)) {
                msg1 = !R.isNil(msg1) ? JSON.toSafeString(msg1) : 'undefined';
            };
            output = util.format('%s:%s %s', level, Reset, msg1);
        } else {
            output = util.format("%s:%s\n", level, Reset);
        }
        switch(level) {
        case "DEBUG":
            console.log(BgMagenta+FgWhite+Bright+output); break;
        case "DEBUGDIR":
            console.log(BgMagenta+FgWhite+Bright+output);
            console.dir(msg1); break;
        case "ERROR":
            console.error(BgRed+FgWhite+Bright+output); break;
        case "WARN":
            console.warn(BgYellow+FgWhite+Bright+output); break;
        case "INFO":
            console.info(BgBlue+FgWhite+Bright+output); break;
        default:
            console.log(BgCyan+FgWhite+Bright+output);
        }
    }
};

logger.info = function() {
    if(process.env.NODE_ENV == 'development') {
        logger.log('INFO', arguments);
    }
};

logger.warn = function() {
    logger.log('WARN', msg1, msg2);
};

logger.error = function() {
    logger.log('ERROR', arguments);
};

logger.debug = function() {
    if(process.env.NODE_ENV == 'development') {
        logger.log('DEBUG', arguments);
    }
};

logger.dir = function() {
    if(process.env.NODE_ENV == 'development') {
        logger.log('DEBUGDIR', arguments);
    }
};
